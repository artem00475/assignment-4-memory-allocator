//
// Created by Артём on 06.12.2022.
//

#include "test_holder.h"

static void print_space() {
    println("--------------------<>--------------------");
}

int main() {
    simple_memory_allocate_and_free();
    print_space();
    free_one_of_several();
    print_space();
    free_two_of_several();
    print_space();
    grow_heap_after_heap();
    print_space();
    grow_heap_anywhere();
}
