//
// Created by Артём on 06.12.2022.
//

#ifndef LAB4_TEST_HOLDER_H
#define LAB4_TEST_HOLDER_H

#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>


void simple_memory_allocate_and_free();

void free_one_of_several();

void free_two_of_several();

void grow_heap_after_heap();

void grow_heap_anywhere();

void println(char *str);

#endif //LAB4_TEST_HOLDER_H
