//
// Created by Артём on 06.12.2022.
//

#include "test_holder.h"

static void free_heap(void *heap, size_t length) {
    munmap(heap, length);
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void println(char *str) {
    printf("%s\n",str);
}

static bool is_simple_memory_allocate_right(void* allocated_mem,struct block_header* header, size_t capacity) {
    //выделен ли блок
    if (!allocated_mem || !header) return 0;
    //выделенный блок нужного размера
    if (header->capacity.bytes<capacity) return 0;
    //блоки правильно заняты аллокатором
    if (header->is_free || !(header->next->is_free)) return 0;
    return 1;
}

static bool is_simple_memory_free_right(struct block_header* header) {
    //выделенный ранее блок особожден
    if (!header->is_free) return 0;
    //куча состоит из одного блока
    if (header->next) return 0;
    return 1;
}

//Тест обычного выделения памяти и ее освобождение
void simple_memory_allocate_and_free() {
    println("TEST 1 STARTED");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    //Выделение памяти с заданным размером
    size_t capacity = 5000;
    void *data = _malloc(capacity);
    debug_heap(stdout, heap);

    //Проверка правильности выделения памяти аллокатором
    struct block_header* header = block_get_header(data);
    if (!is_simple_memory_allocate_right(data,header,capacity)) {
        println("TEST 1 FAILED");
        return;
    }

    //Освобождение памяти и проверка достоверности
    _free(data);
    debug_heap(stdout, heap);

    if (!is_simple_memory_free_right(header)) {
        println("TEST 1 FAILED");
        return;
    }

    size_t heap_current_size = size_from_capacity(header->capacity).bytes;
    free_heap(heap,heap_current_size);

    println("TEST 1 PASSED");
}

static bool is_several_memory_allocate_right(void* allocated_mem1,void* allocated_mem2,void* allocated_mem3,size_t capacity) {
    //Указатели существуют
    if (!allocated_mem1 || !allocated_mem2 || !allocated_mem3) return 0;
    struct block_header* header1 = block_get_header(allocated_mem1);
    struct block_header* header2 = block_get_header(allocated_mem2);
    struct block_header* header3 = block_get_header(allocated_mem3);
    //Заголовки существуют
    if (!header1 || !header2 || !header3) return 0;
    //Блоки знаяты
    if (header1->is_free || header2->is_free || header3->is_free) return 0;
    //Блоки необходимого размера
    if (header1->capacity.bytes<capacity || header2->capacity.bytes<capacity || header3->capacity.bytes<capacity) return 0;
    //Четвертый блок является последним, и он свободен
    if (!header3->next || !header3->next->is_free || header3->next->next) return 0;
    return 1;
}

static bool is_one_of_several_free_right(void* allocated_mem) {
    struct block_header* header = block_get_header(allocated_mem);
    //Блок свободен
    return header->is_free;
}

static bool is_several_memory_free_right(void* allocated_mem) {
    struct block_header* header1 = block_get_header(allocated_mem);
    //В куче два свободных блока
    return header1->is_free && header1->next->is_free && !header1->next->next;
}
//Тест освобождения одного блока из несколькых выделенных
void free_one_of_several() {
    println("TEST 2 STARTED");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    //Выделение трех блоков памяти с заданным размером
    size_t capacity = 3000;
    void *data1 = _malloc(capacity);
    void *data2 = _malloc(capacity);
    void *data3 = _malloc(capacity);
    debug_heap(stdout, heap);

    //Проверка правильности выделения памяти аллокатором
    if (!is_several_memory_allocate_right(data1,data2,data3,capacity)) {
        println("TEST 2 FAILED");
        return;
    }

    //Освобождение одного из выделенных блоков и проверка правильности
    _free(data2);
    debug_heap(stdout, heap);

    if (!is_one_of_several_free_right(data2)) {
        println("TEST 2 FAILED");
        return;
    }

    //Освобождение памяти и проверка достоверности
    _free(data1);
    _free(data3);
    debug_heap(stdout, heap);

    if (!is_several_memory_free_right(data1)) {
        println("TEST 2 FAILED");
        return;
    }

    struct block_header* header1 = block_get_header(data1);
    free_heap(heap, size_from_capacity(header1->capacity).bytes + size_from_capacity(header1->next->capacity).bytes);

    println("TEST 2 PASSED");
}

static bool is_two_of_several_free_right(void* allocated_mem1,void* allocated_mem3) {
    struct block_header* header1 = block_get_header(allocated_mem1);
    //Первый и второй блок свободны
    if (!header1->is_free) return 0;
    struct block_header* header3 = block_get_header(allocated_mem3);
    //Третий блок занят, четвертый свободен
    if (!header3->next->is_free || header3->is_free) return 0;
    //Нет лишнего блока в конце
    if (header3->next->next) return 0;
    return 1;
}
//Тест освобождения двух блоков из нескольких выделенных
void free_two_of_several() {
    println("TEST 3 STARTED");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    //Выделение трех блоков памяти с заданным размером
    size_t capacity = 3000;
    void *data1 = _malloc(capacity);
    void *data2 = _malloc(capacity);
    void *data3 = _malloc(capacity);
    debug_heap(stdout, heap);

    //Проверка правильности выделения памяти аллокатором
    if (!is_several_memory_allocate_right(data1,data2,data3,capacity)) {
        println("TEST 3 FAILED");
        return;
    }

    //Освобождение двух блоков из выделенных и проверка правильности
    _free(data2);
    _free(data1);
    debug_heap(stdout, heap);

    if (!is_two_of_several_free_right(data1,data3)) {
        println("TEST 3 FAILED");
        return;
    }

    //Освобождение памяти и проверка достоверности
    _free(data3);
    debug_heap(stdout, heap);

    if (!is_several_memory_free_right(data1)) {
        println("TEST 3 FAILED");
        return;
    }

    struct block_header* header1 = block_get_header(data1);
    size_t heap_current_size = size_from_capacity(header1->capacity).bytes + size_from_capacity(header1->next->capacity).bytes;
    free_heap(heap, heap_current_size);

    println("TEST 3 PASSED");
}

static bool is_grow_heap_after_heap_right(void* allocated_mem,struct block_header *header, size_t capacity) {
    //Указатель существует
    if (!allocated_mem) return 0;
    //Первый юлок занят, второй свободен
    if (header->is_free || !header->next->is_free) return 0;
    //Выделился блок нужного размера
    if (header->capacity.bytes<capacity*2) return 0;
    //Блоков 2
    if (header->next->next) return 0;
    return 1;
}
//Тест с добавлением памяти, расширяя существующий регион
void grow_heap_after_heap() {
    println("TEST 4 STARTED");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    struct block_header *header = heap;
    size_t capacity = header->capacity.bytes;

    //Выделение блока с недостатком памяти
    void *data = _malloc(capacity * 2);
    debug_heap(stdout, heap);

    //Проверка правильности выделения памяти
    if (!is_grow_heap_after_heap_right(data,header,capacity)) {
        println("TEST 4 FAILED");
        return;
    }

    //Освобождение памяти и проверка достоверности
    _free(data);
    debug_heap(stdout,heap);

    if (!is_simple_memory_free_right(header)) {
        println("TEST 4 FAILED");
        return;
    }
    size_t heap_current_size = size_from_capacity(header->capacity).bytes + size_from_capacity(block_get_header(data)->capacity).bytes;
    free_heap(heap, heap_current_size);

    println("TEST 4 PASSED");
}

static bool is_grow_heap_anywhere_right(void* allocated_mem,struct block_header const *header1,struct block_header *header2, size_t capacity) {
    //Указатель существует
    if (!allocated_mem) return 0;
    //Первый блок свободен и существует следующий
    if (!header1->is_free || !header1->next) return 0;
    //Второй блок занят
    if (header2->is_free) return 0;
    //Второй блок нужного размера
    if (header2->capacity.bytes<capacity*2) return 0;
    return 1;
}

static bool is_grow_heap_anywhere_free_right(struct block_header const *header) {
    //Два блока свободны
    if (!header->is_free || !header->next->is_free) return 0;
    //Всего два блока
    if (header->next->next) return 0;
    return 1;
}
//Тест с добавлением памяти, когда расширение текущего региона невозможно
void grow_heap_anywhere() {
    println("TEST 5 STARTED");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);


    struct block_header const *header = heap;
    size_t capacity = header->capacity.bytes;
    size_t size = size_from_capacity(header->capacity).bytes;

    //Занимаем память следующую за текущим регионом
    void* region = mmap((void *) (header->contents+header->capacity.bytes), REGION_MIN_SIZE,PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    if (!heap || region == MAP_FAILED) {
        println("TEST 5 FAILED");
        return;
    }

    //Выделение блока с недостатком памяти
    void *data = _malloc(capacity*2);
    debug_heap(stdout, heap);
    struct block_header* data_header = block_get_header(data);

    //Проверка правильности выделения памяти
    if (!is_grow_heap_anywhere_right(data,header,data_header,capacity)) {
        println("TEST 5 FAILED");
        return;
    }

    //Освобождение памяти и проверка достоверности
    _free(data);
    debug_heap(stdout, heap);

    if (!is_grow_heap_anywhere_free_right(header)) {
        println("TEST 5 FAILED");
        return;
    }

    free_heap(heap, size+REGION_MIN_SIZE);
    size_t heap_current_size = size_from_capacity(data_header->capacity).bytes;
    free_heap(data_header,heap_current_size);

    println("TEST 5 PASSED");
}
